package xmlrpc;

import org.apache.xmlrpc.WebServer;

public class JavaServer {

	// this function will be called
	public Integer sum(int x, int y) {
		return new Integer(x + y);
	}

	public static void main(String[] args) {

		try {
			WebServer server = new WebServer(80);
			server.addHandler("sample", new JavaServer());
			server.start();

			System.out.println("Server started.");

		} catch (Exception exception) {
			System.err.println("JavaServer: " + exception);
		}
	}
}
